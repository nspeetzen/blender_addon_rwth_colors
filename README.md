# RWTH Colors for Blender

To install download the `blender_addon_rwth_colors.py`, open Blender -> Edit/Preferences -> Add-ons -> Install and select the file.
If this does not add the addon to the list, you can manually paste the file into the blender addons folder (default: `~/.config/blender/YOUR_VERSION/scripts/addons/blender_addon_rwth_colors.py`).
Make sure to check the checkbox to enable the addon.

The colors are displayed in a grid within a new viewport tab on the right side (open sidebar with 'n' by default).
You can hover + Ctrl+C them or drag and drop them into color fields. The names of the colors are shown when hovered.

![image](where_are_colors.png)
