bl_info = {
    "name": "RWTH Color Palette",
    "blender": (2, 80, 0),
    "category": "Utility",
}

import bpy
from mathutils import Color

rwth_palette = {
    "blue": Color((0.0 / 255.0, 84.0 / 255.0, 159.0 / 255.0)),
    "black": Color((0.0 / 255.0, 0.0 / 255.0, 0.0 / 255.0)),
    "magenta": Color((227.0 / 255.0, 0.0 / 255.0, 102.0 / 255.0)),
    "yellow": Color((255.0 / 255.0, 237.0 / 255.0, 0.0 / 255.0)),
    "petrol": Color((0.0 / 255.0, 97.0 / 255.0, 101.0 / 255.0)),
    "teal": Color((0.0 / 255.0, 152.0 / 255.0, 161.0 / 255.0)),
    "green": Color((87.0 / 255.0, 171.0 / 255.0, 39.0 / 255.0)),
    "may_green": Color((189.0 / 255.0, 205.0 / 255.0, 0.0 / 255.0)),
    "orange": Color((246.0 / 255.0, 168.0 / 255.0, 0.0 / 255.0)),
    "red": Color((204.0 / 255.0, 7.0 / 255.0, 30.0 / 255.0)),
    "bordeaux": Color((161.0 / 255.0, 16.0 / 255.0, 53.0 / 255.0)),
    "purple": Color((97.0 / 255.0, 33.0 / 255.0, 88.0 / 255.0)),
    "lilac": Color((122.0 / 255.0, 111.0 / 255.0, 172.0 / 255.0)),
    "white": Color((255.0 / 255.0, 255.0 / 255.0, 255.0 / 255.0)),
    "blue_100": Color((0.0 / 255.0, 84.0 / 255.0, 159.0 / 255.0)),
    "blue_75": Color((64.0 / 255.0, 127.0 / 255.0, 183.0 / 255.0)),
    "blue_50": Color((142.0 / 255.0, 186.0 / 255.0, 229.0 / 255.0)),
    "blue_25": Color((199.0 / 255.0, 221.0 / 255.0, 242.0 / 255.0)),
    "blue_10": Color((232.0 / 255.0, 241.0 / 255.0, 250.0 / 255.0)),
    "black_100": Color((0.0 / 255.0, 0.0 / 255.0, 0.0 / 255.0)),
    "black_75": Color((100.0 / 255.0, 101.0 / 255.0, 103.0 / 255.0)),
    "black_50": Color((156.0 / 255.0, 158.0 / 255.0, 159.0 / 255.0)),
    "black_25": Color((207.0 / 255.0, 209.0 / 255.0, 210.0 / 255.0)),
    "black_10": Color((236.0 / 255.0, 237.0 / 255.0, 237.0 / 255.0)),
    "magenta_100": Color((227.0 / 255.0, 0.0 / 255.0, 102.0 / 255.0)),
    "magenta_75": Color((233.0 / 255.0, 96.0 / 255.0, 136.0 / 255.0)),
    "magenta_50": Color((241.0 / 255.0, 158.0 / 255.0, 177.0 / 255.0)),
    "magenta_25": Color((249.0 / 255.0, 210.0 / 255.0, 218.0 / 255.0)),
    "magenta_10": Color((253.0 / 255.0, 238.0 / 255.0, 240.0 / 255.0)),
    "yellow_100": Color((255.0 / 255.0, 237.0 / 255.0, 0.0 / 255.0)),
    "yellow_75": Color((255.0 / 255.0, 240.0 / 255.0, 85.0 / 255.0)),
    "yellow_50": Color((255.0 / 255.0, 245.0 / 255.0, 155.0 / 255.0)),
    "yellow_25": Color((255.0 / 255.0, 250.0 / 255.0, 209.0 / 255.0)),
    "yellow_10": Color((255.0 / 255.0, 253.0 / 255.0, 238.0 / 255.0)),
    "petrol_100": Color((0.0 / 255.0, 97.0 / 255.0, 101.0 / 255.0)),
    "petrol_75": Color((45.0 / 255.0, 127.0 / 255.0, 131.0 / 255.0)),
    "petrol_50": Color((125.0 / 255.0, 164.0 / 255.0, 167.0 / 255.0)),
    "petrol_25": Color((191.0 / 255.0, 208.0 / 255.0, 209.0 / 255.0)),
    "petrol_10": Color((230.0 / 255.0, 236.0 / 255.0, 236.0 / 255.0)),
    "teal_100": Color((0.0 / 255.0, 152.0 / 255.0, 161.0 / 255.0)),
    "teal_75": Color((0.0 / 255.0, 177.0 / 255.0, 183.0 / 255.0)),
    "teal_50": Color((137.0 / 255.0, 204.0 / 255.0, 207.0 / 255.0)),
    "teal_25": Color((202.0 / 255.0, 231.0 / 255.0, 231.0 / 255.0)),
    "teal_10": Color((235.0 / 255.0, 246.0 / 255.0, 246.0 / 255.0)),
    "green_100": Color((87.0 / 255.0, 171.0 / 255.0, 39.0 / 255.0)),
    "green_75": Color((141.0 / 255.0, 192.0 / 255.0, 96.0 / 255.0)),
    "green_50": Color((184.0 / 255.0, 214.0 / 255.0, 152.0 / 255.0)),
    "green_25": Color((221.0 / 255.0, 235.0 / 255.0, 206.0 / 255.0)),
    "green_10": Color((242.0 / 255.0, 247.0 / 255.0, 236.0 / 255.0)),
    "may_green_100": Color((189.0 / 255.0, 205.0 / 255.0, 0.0 / 255.0)),
    "may_green_75": Color((208.0 / 255.0, 217.0 / 255.0, 92.0 / 255.0)),
    "may_green_50": Color((224.0 / 255.0, 230.0 / 255.0, 154.0 / 255.0)),
    "may_green_25": Color((240.0 / 255.0, 243.0 / 255.0, 208.0 / 255.0)),
    "may_green_10": Color((249.0 / 255.0, 250.0 / 255.0, 237.0 / 255.0)),
    "orange_100": Color((246.0 / 255.0, 168.0 / 255.0, 0.0 / 255.0)),
    "orange_75": Color((250.0 / 255.0, 190.0 / 255.0, 80.0 / 255.0)),
    "orange_50": Color((253.0 / 255.0, 212.0 / 255.0, 143.0 / 255.0)),
    "orange_25": Color((254.0 / 255.0, 234.0 / 255.0, 201.0 / 255.0)),
    "orange_10": Color((255.0 / 255.0, 247.0 / 255.0, 234.0 / 255.0)),
    "red_100": Color((204.0 / 255.0, 7.0 / 255.0, 30.0 / 255.0)),
    "red_75": Color((216.0 / 255.0, 92.0 / 255.0, 65.0 / 255.0)),
    "red_50": Color((230.0 / 255.0, 150.0 / 255.0, 121.0 / 255.0)),
    "red_25": Color((243.0 / 255.0, 205.0 / 255.0, 187.0 / 255.0)),
    "red_10": Color((250.0 / 255.0, 235.0 / 255.0, 227.0 / 255.0)),
    "bordeaux_100": Color((161.0 / 255.0, 16.0 / 255.0, 53.0 / 255.0)),
    "bordeaux_75": Color((182.0 / 255.0, 82.0 / 255.0, 86.0 / 255.0)),
    "bordeaux_50": Color((205.0 / 255.0, 139.0 / 255.0, 135.0 / 255.0)),
    "bordeaux_25": Color((229.0 / 255.0, 197.0 / 255.0, 192.0 / 255.0)),
    "bordeaux_10": Color((245.0 / 255.0, 232.0 / 255.0, 229.0 / 255.0)),
    "purple_100": Color((97.0 / 255.0, 33.0 / 255.0, 88.0 / 255.0)),
    "purple_75": Color((131.0 / 255.0, 78.0 / 255.0, 117.0 / 255.0)),
    "purple_50": Color((168.0 / 255.0, 133.0 / 255.0, 158.0 / 255.0)),
    "purple_25": Color((210.0 / 255.0, 192.0 / 255.0, 205.0 / 255.0)),
    "purple_10": Color((237.0 / 255.0, 229.0 / 255.0, 234.0 / 255.0)),
    "lilac_100": Color((122.0 / 255.0, 111.0 / 255.0, 172.0 / 255.0)),
    "lilac_75": Color((155.0 / 255.0, 145.0 / 255.0, 193.0 / 255.0)),
    "lilac_50": Color((188.0 / 255.0, 181.0 / 255.0, 215.0 / 255.0)),
    "lilac_25": Color((222.0 / 255.0, 218.0 / 255.0, 235.0 / 255.0)),
    "lilac_10": Color((242.0 / 255.0, 240.0 / 255.0, 247.0 / 255.0)),
}

for name, color in rwth_palette.items():
    rwth_palette[name] = Color.from_srgb_to_scene_linear(color)

# disable setting the colors
def set_constant_color(self, value):
    pass

class RWTHColorsPropertyGroup(bpy.types.PropertyGroup):
    blue_100: bpy.props.FloatVectorProperty(name="blue_100",subtype='COLOR',size=3,default=rwth_palette["blue_100"],set=set_constant_color)
    blue_75: bpy.props.FloatVectorProperty(name="blue_75",subtype='COLOR',size=3,default=rwth_palette["blue_75"],set=set_constant_color)
    blue_50: bpy.props.FloatVectorProperty(name="blue_50",subtype='COLOR',size=3,default=rwth_palette["blue_50"],set=set_constant_color)
    blue_25: bpy.props.FloatVectorProperty(name="blue_25",subtype='COLOR',size=3,default=rwth_palette["blue_25"],set=set_constant_color)
    blue_10: bpy.props.FloatVectorProperty(name="blue_10",subtype='COLOR',size=3,default=rwth_palette["blue_10"],set=set_constant_color)
    black_100: bpy.props.FloatVectorProperty(name="black_100",subtype='COLOR',size=3,default=rwth_palette["black_100"],set=set_constant_color)
    black_75: bpy.props.FloatVectorProperty(name="black_75",subtype='COLOR',size=3,default=rwth_palette["black_75"],set=set_constant_color)
    black_50: bpy.props.FloatVectorProperty(name="black_50",subtype='COLOR',size=3,default=rwth_palette["black_50"],set=set_constant_color)
    black_25: bpy.props.FloatVectorProperty(name="black_25",subtype='COLOR',size=3,default=rwth_palette["black_25"],set=set_constant_color)
    black_10: bpy.props.FloatVectorProperty(name="black_10",subtype='COLOR',size=3,default=rwth_palette["black_10"],set=set_constant_color)
    magenta_100: bpy.props.FloatVectorProperty(name="magenta_100",subtype='COLOR',size=3,default=rwth_palette["magenta_100"],set=set_constant_color)
    magenta_75: bpy.props.FloatVectorProperty(name="magenta_75",subtype='COLOR',size=3,default=rwth_palette["magenta_75"],set=set_constant_color)
    magenta_50: bpy.props.FloatVectorProperty(name="magenta_50",subtype='COLOR',size=3,default=rwth_palette["magenta_50"],set=set_constant_color)
    magenta_25: bpy.props.FloatVectorProperty(name="magenta_25",subtype='COLOR',size=3,default=rwth_palette["magenta_25"],set=set_constant_color)
    magenta_10: bpy.props.FloatVectorProperty(name="magenta_10",subtype='COLOR',size=3,default=rwth_palette["magenta_10"],set=set_constant_color)
    yellow_100: bpy.props.FloatVectorProperty(name="yellow_100",subtype='COLOR',size=3,default=rwth_palette["yellow_100"],set=set_constant_color)
    yellow_75: bpy.props.FloatVectorProperty(name="yellow_75",subtype='COLOR',size=3,default=rwth_palette["yellow_75"],set=set_constant_color)
    yellow_50: bpy.props.FloatVectorProperty(name="yellow_50",subtype='COLOR',size=3,default=rwth_palette["yellow_50"],set=set_constant_color)
    yellow_25: bpy.props.FloatVectorProperty(name="yellow_25",subtype='COLOR',size=3,default=rwth_palette["yellow_25"],set=set_constant_color)
    yellow_10: bpy.props.FloatVectorProperty(name="yellow_10",subtype='COLOR',size=3,default=rwth_palette["yellow_10"],set=set_constant_color)
    petrol_100: bpy.props.FloatVectorProperty(name="petrol_100",subtype='COLOR',size=3,default=rwth_palette["petrol_100"],set=set_constant_color)
    petrol_75: bpy.props.FloatVectorProperty(name="petrol_75",subtype='COLOR',size=3,default=rwth_palette["petrol_75"],set=set_constant_color)
    petrol_50: bpy.props.FloatVectorProperty(name="petrol_50",subtype='COLOR',size=3,default=rwth_palette["petrol_50"],set=set_constant_color)
    petrol_25: bpy.props.FloatVectorProperty(name="petrol_25",subtype='COLOR',size=3,default=rwth_palette["petrol_25"],set=set_constant_color)
    petrol_10: bpy.props.FloatVectorProperty(name="petrol_10",subtype='COLOR',size=3,default=rwth_palette["petrol_10"],set=set_constant_color)
    teal_100: bpy.props.FloatVectorProperty(name="teal_100",subtype='COLOR',size=3,default=rwth_palette["teal_100"],set=set_constant_color)
    teal_75: bpy.props.FloatVectorProperty(name="teal_75",subtype='COLOR',size=3,default=rwth_palette["teal_75"],set=set_constant_color)
    teal_50: bpy.props.FloatVectorProperty(name="teal_50",subtype='COLOR',size=3,default=rwth_palette["teal_50"],set=set_constant_color)
    teal_25: bpy.props.FloatVectorProperty(name="teal_25",subtype='COLOR',size=3,default=rwth_palette["teal_25"],set=set_constant_color)
    teal_10: bpy.props.FloatVectorProperty(name="teal_10",subtype='COLOR',size=3,default=rwth_palette["teal_10"],set=set_constant_color)
    green_100: bpy.props.FloatVectorProperty(name="green_100",subtype='COLOR',size=3,default=rwth_palette["green_100"],set=set_constant_color)
    green_75: bpy.props.FloatVectorProperty(name="green_75",subtype='COLOR',size=3,default=rwth_palette["green_75"],set=set_constant_color)
    green_50: bpy.props.FloatVectorProperty(name="green_50",subtype='COLOR',size=3,default=rwth_palette["green_50"],set=set_constant_color)
    green_25: bpy.props.FloatVectorProperty(name="green_25",subtype='COLOR',size=3,default=rwth_palette["green_25"],set=set_constant_color)
    green_10: bpy.props.FloatVectorProperty(name="green_10",subtype='COLOR',size=3,default=rwth_palette["green_10"],set=set_constant_color)
    may_green_100: bpy.props.FloatVectorProperty(name="may_green_100",subtype='COLOR',size=3,default=rwth_palette["may_green_100"],set=set_constant_color)
    may_green_75: bpy.props.FloatVectorProperty(name="may_green_75",subtype='COLOR',size=3,default=rwth_palette["may_green_75"],set=set_constant_color)
    may_green_50: bpy.props.FloatVectorProperty(name="may_green_50",subtype='COLOR',size=3,default=rwth_palette["may_green_50"],set=set_constant_color)
    may_green_25: bpy.props.FloatVectorProperty(name="may_green_25",subtype='COLOR',size=3,default=rwth_palette["may_green_25"],set=set_constant_color)
    may_green_10: bpy.props.FloatVectorProperty(name="may_green_10",subtype='COLOR',size=3,default=rwth_palette["may_green_10"],set=set_constant_color)
    orange_100: bpy.props.FloatVectorProperty(name="orange_100",subtype='COLOR',size=3,default=rwth_palette["orange_100"],set=set_constant_color)
    orange_75: bpy.props.FloatVectorProperty(name="orange_75",subtype='COLOR',size=3,default=rwth_palette["orange_75"],set=set_constant_color)
    orange_50: bpy.props.FloatVectorProperty(name="orange_50",subtype='COLOR',size=3,default=rwth_palette["orange_50"],set=set_constant_color)
    orange_25: bpy.props.FloatVectorProperty(name="orange_25",subtype='COLOR',size=3,default=rwth_palette["orange_25"],set=set_constant_color)
    orange_10: bpy.props.FloatVectorProperty(name="orange_10",subtype='COLOR',size=3,default=rwth_palette["orange_10"],set=set_constant_color)
    red_100: bpy.props.FloatVectorProperty(name="red_100",subtype='COLOR',size=3,default=rwth_palette["red_100"],set=set_constant_color)
    red_75: bpy.props.FloatVectorProperty(name="red_75",subtype='COLOR',size=3,default=rwth_palette["red_75"],set=set_constant_color)
    red_50: bpy.props.FloatVectorProperty(name="red_50",subtype='COLOR',size=3,default=rwth_palette["red_50"],set=set_constant_color)
    red_25: bpy.props.FloatVectorProperty(name="red_25",subtype='COLOR',size=3,default=rwth_palette["red_25"],set=set_constant_color)
    red_10: bpy.props.FloatVectorProperty(name="red_10",subtype='COLOR',size=3,default=rwth_palette["red_10"],set=set_constant_color)
    bordeaux_100: bpy.props.FloatVectorProperty(name="bordeaux_100",subtype='COLOR',size=3,default=rwth_palette["bordeaux_100"],set=set_constant_color)
    bordeaux_75: bpy.props.FloatVectorProperty(name="bordeaux_75",subtype='COLOR',size=3,default=rwth_palette["bordeaux_75"],set=set_constant_color)
    bordeaux_50: bpy.props.FloatVectorProperty(name="bordeaux_50",subtype='COLOR',size=3,default=rwth_palette["bordeaux_50"],set=set_constant_color)
    bordeaux_25: bpy.props.FloatVectorProperty(name="bordeaux_25",subtype='COLOR',size=3,default=rwth_palette["bordeaux_25"],set=set_constant_color)
    bordeaux_10: bpy.props.FloatVectorProperty(name="bordeaux_10",subtype='COLOR',size=3,default=rwth_palette["bordeaux_10"],set=set_constant_color)
    purple_100: bpy.props.FloatVectorProperty(name="purple_100",subtype='COLOR',size=3,default=rwth_palette["purple_100"],set=set_constant_color)
    purple_75: bpy.props.FloatVectorProperty(name="purple_75",subtype='COLOR',size=3,default=rwth_palette["purple_75"],set=set_constant_color)
    purple_50: bpy.props.FloatVectorProperty(name="purple_50",subtype='COLOR',size=3,default=rwth_palette["purple_50"],set=set_constant_color)
    purple_25: bpy.props.FloatVectorProperty(name="purple_25",subtype='COLOR',size=3,default=rwth_palette["purple_25"],set=set_constant_color)
    purple_10: bpy.props.FloatVectorProperty(name="purple_10",subtype='COLOR',size=3,default=rwth_palette["purple_10"],set=set_constant_color)
    lilac_100: bpy.props.FloatVectorProperty(name="lilac_100",subtype='COLOR',size=3,default=rwth_palette["lilac_100"],set=set_constant_color)
    lilac_75: bpy.props.FloatVectorProperty(name="lilac_75",subtype='COLOR',size=3,default=rwth_palette["lilac_75"],set=set_constant_color)
    lilac_50: bpy.props.FloatVectorProperty(name="lilac_50",subtype='COLOR',size=3,default=rwth_palette["lilac_50"],set=set_constant_color)
    lilac_25: bpy.props.FloatVectorProperty(name="lilac_25",subtype='COLOR',size=3,default=rwth_palette["lilac_25"],set=set_constant_color)
    lilac_10: bpy.props.FloatVectorProperty(name="lilac_10",subtype='COLOR',size=3,default=rwth_palette["lilac_10"],set=set_constant_color)

class RWTHColors_VP_Panel(bpy.types.Panel):
    bl_label = "RWTH Colors"
    bl_idname = "RWTH_COLORS_PANEL"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'RWTHColors'
    
    def draw(self, context):
        layout = self.layout
        rwth_colors = context.scene.rwth_colors
         # Create a box to hold the color swatch
        # Display the color swatch in the box
        box = layout.box()
        for color in ["blue", "black", "magenta", "yellow", "petrol", "teal", "green", "may_green", "orange", "red", "bordeaux", "purple", "lilac"]:        
            row = box.row()
            row.prop(rwth_colors, color + "_100", text="", icon='COLOR', expand=False, emboss=True, slider=False)
            row.prop(rwth_colors, color + "_75", text="", icon='COLOR', expand=False, emboss=True, slider=False)
            row.prop(rwth_colors, color + "_50", text="", icon='COLOR', expand=False, emboss=True, slider=False)
            row.prop(rwth_colors, color + "_25", text="", icon='COLOR', expand=False, emboss=True, slider=False)
            row.prop(rwth_colors, color + "_10", text="", icon='COLOR', expand=False, emboss=True, slider=False)


def register():
    bpy.utils.register_class(RWTHColorsPropertyGroup)
    bpy.types.Scene.rwth_colors = bpy.props.PointerProperty(type=RWTHColorsPropertyGroup)
    bpy.utils.register_class(RWTHColors_VP_Panel)

def unregister():
    bpy.utils.unregister_class(RWTHColors_VP_Panel)
    del bpy.types.Scene.rwth_colors
    bpy.utils.unregister_class(RWTHColorsPropertyGroup)


if __name__ == "__main__":
    register()